---
title: Ginkgo Development Infrastructure
author:
    - Terry Cojean
    - Goran Flegar
date: 25 June 2018
---

About
-----

- High performance linear algebra library for manycore architectures focused on solution of linear systems.
- C++11, CUDA for GPU kernels, OpenMP for CPU kernels (not yet complete)
- Contributors from KIT, UJI (Spain), NTU (Taiwan), ICL (US)

Version control (git)
---------------------

- Gitflow workflow (`master`, `develop`, feature branches)
- Two repositories:
    - public on Github for users, contributors, work not related to publications
    - private on Gitlab for research
    - synchronized `master` and `develop` branches
    - distinct develop branches
    - problem: private feature branch has to wait for paper publication
- The "_Microsoft buying Github_" thing
    - maybe move both repositories to Gitlab?
- Community Gitlab vs KIT's Gitlab
    - KIT's version is more stable
    - easier to get an account on community version (external contributors!)
    - Shared CI runners with GPUs on KIT's Gitlab?
    - Repository of Docker images with different versions of common software?
        - compilers, libraries, etc.

Software life cycle
-------------------

- Unit tests (_Google Test_)
- Merge requests and peer reviews (_Gitlab_, _Github_)
- Issues to track bugs and steer development (_Gitlab_, _Github_)
- Wiki for tutorials, guidelines, useful resources (_Github_)
- Build system (_CMake_)
- Documentation (_Doxygen_)
- Code style (_cmake-format_, _git-cmake-format_)

Continuous integration
----------------------

- Gitlab CI
    - Uses `gitlab-ci.yml` configuration file
    - Fairly simple to use
    - Create job specifications (define tags, e.g. GPU), dependencies
    - Use gitlab interface for interaction ((re)run pipelines, access data,...)
- Automatic build with several compilers
    - Build with everything or only with CPU kernel execution.
    - C++11 project: build for both clang, and gcc.
    - Future: multiple versions of them.
- Automatic unit testing
    - Download artifacts from the build step
    - Launches `make test` on top
    - Small problem: test always after *all* bulid, no fine-grain dependencies


Continuous integration
----------------------
- Automatic synchronization of public and private repositories
    - As stated before, commit to `develop`/`master` is through PR only
    - PR acceptation triggers a merge commit
    - Merge commit for those two branches triggers a sync step
    - How it works:
        - _bot_ account with full access
        - `git clone` private repository
        - `git pull --ff-only` the public repository on top
        - If it did not fail (return 0) proceed and push to both.
- Things above give us automatic verification of new merge requests

Continuous integration
----------------------
- Problem: when pipeline succeeds, it does not mean merged pipeline will succeed
    - Functionality conflict 
    - Idea: on commit (merge) to develop, in the CI system do:
        - find out all open MR/PR (~= active branches)
        - try to rebase/merge to the last `develop`
        - if succeeds, launch build/test
        - if it fails, post a comment on the MR/PR? or send mail
- Security: manually invoke pipeline for pull requests from other repositories
    - Script to trigger a pipeline for a PR number.
- Future: 
    - automatically benchmark (every night, or every Sunday)
    - generate documentation and deploy it on the webpage

Our CI setup
------------

- CI server with GPUs
    - Spare that is no longer needed
    - Hosted directly in SCC server room
    - Setup
        - K20 GPUs, 40 cores, 60GB RAM
        - CentOS 7 (same system as SCC's)
    - Useful CentOS repositories for our CI
        - [EPEL](https://fedoraproject.org/wiki/EPEL) provides nvidia kernel modules
        - [CUDA RHEL 7 repository](https://developer.nvidia.com/cuda-downloads?target_os=Linux&target_arch=x86_64&target_distro=CentOS&target_version=7)
        - [Gitlab Runner repository](https://docs.gitlab.com/runner/install/linux-repository.html) 
        - [docker-ce](https://docs.gitlab.com/runner/install/linux-repository.html)
- CI configuration
    - Parallel jobs
    - Create runner on server available to whole project
    - `/tmp` and `/var/lib/docker` are tmpfs (live in RAM)

Our CI setup
------------

- Docker for security and alternative configurations
    - Currently use basic `base/devel` image (arch linux)
    - Install all packages by hand with pacman
    - Errors can be common (e.g. sudden new CUDA version showing incompatibility)
    - Currently testing: special docker images to use GPUs
        - Provides parameters `-runtime=nvidia` to docker and image `nvidia/cuda`
        - Allows to use CUDA/gpus properly, but fixed versions of CUDA to 9.0 and old-ish gcc
        - For every image, other way exists but seems more tricky
    - Future: Create images with different compilers AND CUDA versions
